<!--Footer Start-->

<footer class="footer-style-1 bg-light">
    <div class="container">
       <div class="row mx-auto pb-4">
         <div class="col-md-3 pb-3">
             <img  alt="logo intendencia montevideo" width="120px"  src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-intendencia.png" >
         </div>
         <div class="col-md-3"></div>
         <div class="footer-text col-md-3">
            <p class="mb-1"><b> Secretaría de Infancia, Adolescencia y Juventud </b></p>
            <ul style="list-style-type: none;" class="">   
                        <li><b><i class="fa-solid fa-envelope pr-1"></i>infancia.juventud@imm.gub.uy</b></li>     
                         <li><b><i class="fa-solid fa-location-dot pr-1"></i>Soriano 1426</b></li>               
                         <li><b><i class="fa-solid fa-phone pr-1"></i>1950 8640 / 8641</b></li>
                                       
             </ul>
         </div>
         <div class="col-md-3">
            <div class="footer-social text-center text-lg-left ">
                        

                        <ul class="list-unstyled">
                            <li><a class="wow fadeInUp" href="https://www.facebook.com/MontevideoJoven"><i aria-hidden="true" class="fab fa-facebook-f"></i></a></li>
                            <li><a class="wow fadeInUp" href="https://www.instagram.com/montevideojoven"><i aria-hidden="true" class="fab fa-instagram"></i></a></li>
                            <li><a class="wow fadeInDown" href="https://www.youtube.com/channel/UCnJlMZrHUNGOBwZlZU7yeFg"><i aria-hidden="true" class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
    
            </div>

       </div> 
    </div>
   

  
</footer>
<!--Footer End-->

<!--Scroll Top Start-->
<span class="scroll-top-arrow"><i class="fas fa-angle-up"></i></span>
<!--Scroll Top End-->

<!-- JavaScript -->

<?php wp_footer(); ?>

</body>
</html>