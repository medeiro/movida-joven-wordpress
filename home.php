<?php


get_header();
?>

<!--Home Start-->
<div id="rev-slider" style="margin-top: 64px;" class=" rev-slider">
   <?php get_template_part( 'template-parts/home-slider' );?>
</div>
<!--Home End-->


<section id="inicio">
    <div class="container">
        <div class="row dot-box">

            <!-- Heading Area-->
            <div class="pb-2 col-12 col-lg-6 about-img-area text-center text-lg-left">
                    <div class="heading-area pl-lg-4 p-0">
                   
                        <h2 class="title">#MovidaJoven</h2>
                        
                          <?php if ( is_active_sidebar( 'acerca-inicio-texto' ) ) : ?>   
                           
                              <?php dynamic_sidebar( 'acerca-inicio-texto' ); ?>
                             
                           <?php endif; ?>
                      
                            <!--List-->
                            <a class="btn yellow-and-white-slider-btn" href="<?php echo get_site_url() ?>/acerca">Leer más</a>
                    </div>
            </div>
            <div class="col-12 col-lg-6 about-img-area">
                <div class="about-img">
                    <img alt="logo artistico movida joven" src="<?php echo get_stylesheet_directory_uri() ?>/img/acerca.png">
                </div>
            </div>
        </div>
    </div>
</section>

<!--our cast sec start-->
<section class="our-cast" id="disciplinas">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 text-center">
                <div class="about-heading heading-details">
                    <h4 class="heading">DISCIPLINAS</h4>
                    <p class="top-des"></p>
                </div>
            </div>
        </div>
        <div class="row no-gutters testimonial-cast owl-carousel owl-theme">
        <?php
                                    $categories = get_categories( array(
                                        'orderby' => 'name',
                                        'order'   => 'ASC'
                                    ) );
                                    $par = false;
                                    foreach( $categories as $category ) { if($category->cat_ID !=1){
                                        $par=!$par;
                                        ?>  
                                
            <div class="item">
                <div class="row no-gutters cast-card">
                    
                    <div class="col-12">
                        <?php if($par){?>
                        <div class="cast-img">
                            <img alt="logo disciplina <?php echo $category->slug; ?>" src="<?php echo get_stylesheet_directory_uri();  ?>/img/disciplinas-tarjeta/<?php echo $category->slug; ?>.png">
                        </div>
                        <div class="cast-detail d-flex align-items-center">
                            <div class="cast-content">
                            <h4 class="cast-name"><?php echo $category->name?></h4>
                          
                            <p class="cast-des"><?php echo $category->description?></p>
                            <ul class="cast-social">
                               <a class="btn yellow-and-white-slider-btn" href="<?php  echo esc_url( get_category_link( $category->term_id ) ) ?>">Ver más</a>
                            </ul>
                            </div>
                            <div class="overlay-cast"></div>
                        </div>
                       <?php } else {?>

                      
                        <div class="cast-detail d-flex align-items-center">
                            <div class="cast-content">
                            <h4 class="cast-name"><?php echo $category->name?></h4>
                          
                            <p class="cast-des"><?php echo $category->description?></p>
                            <ul class="cast-social">
                               <a class="btn yellow-and-white-slider-btn" href="<?php  echo esc_url( get_category_link( $category->term_id ) ) ?>">Ver más</a>
                            </ul>
                            </div>
                            <div class="overlay-cast"></div>
                        </div>
                        <div class="cast-img">
                            <img src="<?php echo get_stylesheet_directory_uri();  ?>/img/disciplinas-tarjeta/<?php echo $category->slug; ?>.png">
                        </div>
                       <?php }?>
                    </div>
                </div>
            </div>
            <?php }}?>
        </div> 
      
    </div>
</section>
<!--our cast sec start-->

<section class="bglight padding" id="cronograma">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                    <div class="about-heading heading-details">
                        <h4 class="heading">CRONOGRAMA</h4>
                    </div>
            </div>
        </div>
   
        <div class="row">
        
            <div class="col-lg-12 pb-3">
                 <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/calendario/Calendario_Octubre Noviembre.png"> 
            </div>
           
          
        </div>
    </div>
</section>

<section class="bglight padding" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                    <div class="about-heading heading-details">
                        <h4 class="heading">LAS ULTIMAS NOVEDADES</h4>
                    </div>
            </div>
        </div>
   
        <div class="row">
        <?php    
        // the query
        $the_query = new WP_Query( array( 
            'category_id' => '1', 
            'posts_per_page' => 3 
        ) ); 
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
        ?>

            <div class="col-lg-4 col-md-6">
                <div style="min-height: 677px;" class="news_item shadow wow fadeInUp" data-wow-delay="200ms">
                    <div class="blog-img">
                        <a class="image" href="<?php the_permalink(); ?>">                          
                                   <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                         <img alt="banner posteo <?php the_title(); ?>" class="img-responsive" src="<?php  echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="image">
                                    <?php else: ?>
                                       <img  alt="banner posteo logo artisco movida joven" class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/img/default-post.jpg" alt="image">
                                    <?php endif; ?>
                                            
                        </a>
                    </div>
                    <div class="news_desc">
                        <h3 class="font-normal darkcolor"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                      
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </div>
            </div>
         <?php
            }
          }
         ?>
        </div>
    </div>
</section>

<section id="contacto" style="  background-image:url(<?php echo get_stylesheet_directory_uri() ?>/img/fondo-transparente.png);   background-size: cover; background-repeat: no-repeat; /*background-position: center center;*/ " class="contact-sec">
<div class="container">
        <!--Heading-->
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 text-center">
                <div class="about-heading heading-details">
                    <h4 class="heading">CONTACTANOS</h4>
                </div>
            </div>
        </div>

        <!--contact us-->

            <div class="row">
                <div class="col-12 col-lg-6 contact-details text-center text-lg-left">
                <div class="location-details">
                     
                     <ul class="contact-list">             
                         <li><span>Teléfono :</span>1950 8645</li>
                         <li><span>Email :</span> info.movidajoven@imm.gub.uy</li>                   
                     </ul>
                     <ul class="slider-social contact-s-media">
                         <li class="animated-wrap"><a class="wow fadeInUp" href="https://www.facebook.com/MontevideoJoven"><i aria-hidden="true" class="fab fa-facebook-f"></i></a> </li>
                         <li class="animated-wrap"><a class="wow fadeInUp" href="https://www.instagram.com/montevideojoven"><i aria-hidden="true" class="fab fa-instagram"></i></a> </li>
                         <li class="animated-wrap"><a class="wow fadeInDown" href="https://www.youtube.com/channel/UCnJlMZrHUNGOBwZlZU7yeFg"><i aria-hidden="true" class="fab fa-youtube"></i></a ></li>
                        
                     </ul>
                     
                     </div>
                </div>

                <div class="col-12 text-center col-lg-6 contact-form-box">
                      <p >
                        <b> Suscribete y enterate de las ultimas novedades!</b> <?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
                    <div id="sidebar-footer">
                    <?php dynamic_sidebar( 'sidebar-footer' ); ?>
                    </div>
                    <?php endif; ?>
                        </p>
                        <!--
                    <div style="max-width:400px" class="row mx-auto text-center">  
                        <div class="col-sm-12 " id="result"></div>
          
                        <div class="col-lg-9">
                            <div class="form-group">
                               <input class="form-control" name="EMAIL" type="email" placeholder="Email:" required="" id="email" name="userEmail">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-large yellow-and-white-slider-btn  contact_btn" id="submit_btn"><i class="fa fa-spinner fa-spin mr-2 d-none" aria-hidden="true"></i> <b>Suscribirse</b></button>
                        </div>
                    </div>
    -->
   
  
   </section>
                </div>
            </div>
    </div>
</section>


                    
<?php
get_footer();?>
