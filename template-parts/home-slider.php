<!-- START  -->
<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="slider-shop" data-source="gallery" style="background:rgba(255,255,255,0);padding:0px;">
	<div id="rev_slider_1_5" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
		<ul>
                    <!-- BEGIN SLIDE -->
            <li data-transition="fade">
            
            <!-- required for background video, and will serve as the video's "poster/cover" image -->
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/img-home.jpg"
                alt="Ocean"
                class="rev-slidebg"
                data-bgposition="center center"
                data-bgfit="cover"
                data-bgrepeat="no-repeat">

            <!-- HTML5 BACKGROUND VIDEO LAYER -->
            <div class="rs-background-video-layer"
                data-videomp4="<?php echo get_stylesheet_directory_uri() ?>/video/video-home4.mp4"
                data-videopreload="auto"
                data-volume="mute" 
                data-forcerewind="on"
                data-nextslideatend="false" 
                data-videoloop="loop"
                data-autoplay="on"

            ></div>
            
        <div class="tp-caption tp-resizeme"
        
        data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                    {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'

        data-type="image"
        data-x="center"
        data-y="center"
        data-hoffset="0"
        data-voffset="170"
        data-width="['auto']"
        data-height="['auto']"

        > 
       <!-- <img alt="logo movida joven"   src="<?php echo get_stylesheet_directory_uri() ?>/img/slider-logo.png" ></div>
        <div class="tp-caption rev-btn  rs-parallaxlevel-2"
                         id="slide-3-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['290','290','290','290']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":280,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 11; max-width:960px; white-space: nowrap; font-size: 14px; line-height: 22px;letter-spacing: 3px; font-weight: 500; color: #ffffff; font-family:'Roboto',sans-serif;">
                         <!--<a class="btn yellow-and-white-slider-btn"  href="<?php echo get_site_url() ?>/inscripcion">¡Inscribite!</a>-->
                    </div>

            </li>

		</ul>
		<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div></div>
</div>
<!-- END REVOLUTION SLIDER -->