	<!--Navigation-->
    <nav class="navbar navbar-top-default navbar-expand-lg navbar-simple nav-line">
				<div class="row no-gutters w-100">
				<div class="col-3 pl-3">
					<a href="#inicio" title="Logo" class="logo scroll">
						<!--Logo Default-->
						<img alt="logo intendencia montevideo" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-intendencia.png" alt="logo" class="logo-home logo-dark">
					</a>
				</div>

					<!--Nav Links-->
					<div class="col-6">
					<div class="collapse navbar-collapse" id="megaone">
						<div class="navbar-nav ml-auto mr-auto">
							<a class="nav-link active  <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#inicio">INICIO</a>
							<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#disciplinas">DISCIPLINAS</a>
							<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#cronograma">CRONOGRAMA</a>
							<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#blog">BLOG</a>
							<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#contacto">CONTACTO</a>
						
						</div>
					</div>
					</div>
					<!--Side Menu Button-->
					<div class="col-3 pr-3">
					<a href="javascript:void(0)" class="sidemenu_btn" id="sidemenu_toggle">
						<span></span>
						<span></span>
						<span></span>
					</a>
					</div>

				</div>
			</nav>

			<!--Side Nav-->
			<div class="side-menu hidden">
				<div class="inner-wrapper">
					<span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
					<nav class="side-nav w-100">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link <?php if(is_home()){ echo "scroll";}?> " href="<?php if(!is_home()){echo site_url()."/";} ?>#inicio">INICIO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#disciplinas">DISCIPLINAS</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#cronograma">CRONOGRAMA</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#blog">BLOG</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?php if(is_home()){ echo "scroll";}?>" href="<?php if(!is_home()){echo site_url()."/";} ?>#contacto">CONTACTO</a>
							</li>
				
						</ul>
					</nav>

					<div class="side-footer text-white w-100">
						<ul class="social-icons-simple">
							<li><a class="facebook-text-hvr" href="https://www.facebook.com/MontevideoJoven"><i class="fab fa-facebook-f"></i> </a> </li>
							<li><a class="instagram-text-hvr" href="https://www.instagram.com/montevideojoven"><i class="fab fa-instagram"></i> </a> </li>
							<li><a class="youtube-text-hvr" href="https://www.youtube.com/channel/UCnJlMZrHUNGOBwZlZU7yeFg"><i class="fab fa-youtube"></i> </a> </li>
						
						</ul>
						
							<div style="max-width:200px" class="row mr-0 align-items-center">
									<div class="col">
										<img alt="logo intendencia de montevideo"  width="80px"  src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-intendencia.png" >
									</div>
									<div class="col">
										<img alt="logo movida joven"   width="80px" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-movida.png" >
									</div>
								</div>
						
					</div>
				</div>
			</div>
			<a id="close_side_menu" href="javascript:void(0);"></a>
			<!-- End side menu -->