<?php
get_header();

?>

<!--slider sec strat-->
<section id="slider-sec" class="slider-sec slider-sec-post-cattegory parallax" style="background: url('<?php echo get_stylesheet_directory_uri() ?>/img/category-slider.jpg');">
    <div class="overlay text-center d-flex justify-content-center align-items-center">
     
        <div class="slide-contain">
           <img width="250px" alt="imagen dejate llevar" src="<?php echo get_stylesheet_directory_uri() ?>/img/dejate-llevar.png" >
           <p class="mt-3" style="color:white"><b><?php  echo get_post()->post_title;?></b></p>
          
        </div>
    </div>
</section>
<!--slider sec end-->


<!--main page content-->
<section  class="main" id="main">
    <!--content-->
    <div style="max-width:800px" class="mx-auto blog-content">
        <div class="container mb-5">
            <div class="row">
                <div class="col-12">
                    <div class="main_content text-center text-lg-left">
                        <div class="detail_blog">
                            <div class="blog_detail">
                              
                                <?php  the_content(); ?>

                            </div>
                        </div>
                     

                    </div>

                </div>
               
            </div>
        </div>
    </div>
</section>
<!--main page content end-->


<?php

get_footer();
?>