<?php

function anadir_css(){

    wp_enqueue_style('1',get_stylesheet_directory_uri().'/vendor/css/bundle.min.css');
    
    wp_enqueue_style('2',get_stylesheet_directory_uri().'/css/line-awesome.min.css');
    wp_enqueue_style('3',get_stylesheet_directory_uri().'/vendor/css/revolution-settings.min.css');
    wp_enqueue_style('4',get_stylesheet_directory_uri().'/vendor/css/jquery.fancybox.min.css');
    wp_enqueue_style('5',get_stylesheet_directory_uri().'/vendor/css/owl.carousel.min.css');
    wp_enqueue_style('6',get_stylesheet_directory_uri().'/vendor/css/cubeportfolio.min.css');

  
    wp_enqueue_style('7',get_stylesheet_directory_uri().'/css/slick.css');
    wp_enqueue_style('8',get_stylesheet_directory_uri().'/css/slick-theme.css');
    wp_enqueue_style('9',get_stylesheet_directory_uri().'/css/navigation.css');
    wp_enqueue_style('10',get_stylesheet_directory_uri().'/css/blog.css');
    wp_enqueue_style('11',get_stylesheet_directory_uri().'/css/style.css');
    wp_enqueue_style('12',get_stylesheet_directory_uri().'/vendor/fontawesome/css/all.css');

  /*
    
    <link href="" rel="icon">
        <!-- Bundle -->
        <link href="../vendor/css/bundle.min.css" rel="stylesheet">
        <!-- Plugin Css -->
        <link href="css/line-awesomse.min.css" rel="stylesheet">
        <link href="../vendor/css/revolution-settings.min.cs" rel="stylesheet">
        <link href="../vendor/css/jquery.fancybox.min.css" rel="stylesheet">
        <link href="../vendor/css/owl.carousel.min.css" rel="stylesheet">
        <link href="../vendor/css/cubeportfolio.min.css" rel="stylesheet">
        <link href="css/slick.css" rel="stylesheet">
        <link href="css/slick-theme.css" rel="stylesheet">
        <!-- Style Sheet -->
        <link href="css/navigation.css" rel="stylesheet">
        <link href="css/blog.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        */
}

add_action('wp_enqueue_scripts',"anadir_css");


function anadir_js() {
    /*
            <!-- JavaScript -->
        <script src="../vendor/js/bundle.min.js"></script>
        <!-- Plugin Js -->
        <script src="../vendor/js/jquery.appear.js"></script>
        <script src="../vendor/js/jquery.fancybox.min.js"></script>
        <script src="../vendor/js/owl.carousel.min.js"></script>
        <script src="../vendor/js/parallaxie.min.js"></script>
        <script src="../vendor/js/wow.min.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script src="../vendor/js/jquery.themepunch.tools.min.js"></script>
        <script src="../vendor/js/jquery.themepunch.revolution.min.js"></script>
        <script src="../vendor/js/jquery.cubeportfolio.min.js"></script>
        <!-- SLIDER REVOLUTION EXTENSIONS -->
        <script src="../vendor/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="../vendor/js/extensions/revolution.extension.video.min.js"></script>
        <!-- google map-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJRG4KqGVNvAPY4UcVDLcLNXMXk2ktNfY"></script>
        <script src="js/map.js"></script>
        <!--Tilt Js-->
        <script src="js/tilt.jquery.min.js"></script>
        <script src="js/slick.js"></script>
        <script src="js/slick.min.js"></script>
        <!-- custom script-->
        <script src="js/script.js"></script>

    */
    wp_enqueue_script('1', get_stylesheet_directory_uri().'/vendor/js/bundle.min.js');
    wp_enqueue_script('2', get_stylesheet_directory_uri().'/vendor/js/jquery.appear.js');
    wp_enqueue_script('3', get_stylesheet_directory_uri().'/vendor/js/jquery.fancybox.min.js');
    wp_enqueue_script('4', get_stylesheet_directory_uri().'/vendor/js/owl.carousel.min.js');
    wp_enqueue_script('5', get_stylesheet_directory_uri().'/vendor/js/parallaxie.min.js');
    wp_enqueue_script('6', get_stylesheet_directory_uri().'/vendor/js/wow.min.js');
    wp_enqueue_script('7', get_stylesheet_directory_uri().'/vendor/js/jquery.themepunch.tools.min.js');
    wp_enqueue_script('8', get_stylesheet_directory_uri().'/vendor/js/jquery.themepunch.revolution.min.js');
    wp_enqueue_script('9', get_stylesheet_directory_uri().'/vendor/js/jquery.cubeportfolio.min.js');
    wp_enqueue_script('10', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.actions.min.js');
    wp_enqueue_script('11', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.carousel.min.js');
    wp_enqueue_script('12', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.kenburn.min.js');
    wp_enqueue_script('13', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.layeranimation.min.js');
    wp_enqueue_script('14', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.migration.min.js');
    wp_enqueue_script('15', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.navigation.min.js');
    wp_enqueue_script('16', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.parallax.min.js');
    wp_enqueue_script('17', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.slideanims.min.js');
    wp_enqueue_script('18', get_stylesheet_directory_uri().'/vendor/js/extensions/revolution.extension.video.min.js');
  
    wp_enqueue_script('19', "https://maps.googleapis.com/maps/api/js?key=AIzaSyCJRG4KqGVNvAPY4UcVDLcLNXMXk2ktNfY");
    wp_enqueue_script('20', get_stylesheet_directory_uri().'/js/map.js');
    wp_enqueue_script('21', get_stylesheet_directory_uri().'/js/slick.js');
    wp_enqueue_script('22', get_stylesheet_directory_uri().'/js/slick.min.js');
    wp_enqueue_script('23', get_stylesheet_directory_uri().'/js/script.js');
  
}

/* Custom script with no dependencies, enqueued in the header */
add_action('wp_head', 'anadir_js');

add_action( 'widgets_init', 'register_new_sidebars' );
function register_new_sidebars() {
    register_sidebar(array(
        'name' => __('acerca inicio texto'),
        'id' => 'acerca-inicio-texto',
        'before_widget' => '<p class="paragraph">',
        'after_widget' => '</p>',
        ));
    register_sidebar(array(
    'name' => __('Pie de página'),
    'id' => 'sidebar-footer',
    'before_widget' => '<div class="widget-footer">',
    'after_widget' => '</div>',
    //'before_title' => '<h4>',
    //'after_title' => '</h4>'
    ));
}

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
    