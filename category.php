<?php
get_header();

$query2 = new WP_Query(  array( 'pagename' => $wp_query->query_vars['category_name'].'_cat' )  );

?>

<!--slider sec strat-->
<section id="slider-sec" class=" slider-sec-post-cattegory slider-sec parallax" style="background: url('<?php echo get_stylesheet_directory_uri() ?>/img/categorias/fondo-<?php echo $wp_query->query_vars['category_name'];?>.png');">
    <div class="overlay text-center d-flex justify-content-center align-items-center">
     
        <div class="slide-contain">
           <img width="550px" alt="logo de la categoria <?php echo $wp_query->query_vars['category_name'];?>" src="<?php echo get_stylesheet_directory_uri() ?>/img/categorias/<?php echo $wp_query->query_vars['category_name'];?>.png" >
          
          
        </div>
    </div>
</section>
<!--slider sec end-->


<!--main page content-->
<section class="main" id="main">
    <!--content-->
    <div class="blog-content">
        <div class="container mb-5">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="main_content text-center text-lg-left">
                        <div class="detail_blog">
                            <div class="blog_detail">
                            <?php                              
                                if($query2->have_posts()) : 
                                    while($query2->have_posts()) : 
                                        $query2->the_post();
                                ?>                                       
                                        <?php the_content() ?>    
                                <?php
                                    endwhile;
                                else: 
                                ?>
                                    Vaya, no hay entradas.
                                <?php
                                endif;
                                ?>
                               
                            </div>
                        </div>
                     

                    </div>

                </div>
                <div class="col-12 col-lg-4">
                    <div class="row">
                        <div class="col-12">
                          
                            <div class="cat_sec">
                                <h4 class="text-center text-lg-left">Disciplinas</h4>
                                <ul>
                                <?php
                                    $categories = get_categories( array(
                                        'orderby' => 'name',
                                        'order'   => 'ASC'
                                    ) );
                                    
                                    foreach( $categories as $category ) { if($category->cat_ID !=1){ ?>  
                                      <li><a href="<?php  echo esc_url( get_category_link( $category->term_id ) ) ?>"><?php echo $category->name?></a> </li>                             
                                   <?php }}?>
                                </ul>
                            </div>
                            <div class="popular_posts">
                                <h4 class="text-center text-lg-left">Blog</h4>
                                <?php
                                  
                                    while($wp_query -> have_posts()) : $wp_query -> the_post(); ?>

                                     

                                   
                                    <div  class="media-box row">
                                       
                                            <div class="col-5 box-img">
                                              <a href="<?php the_permalink(); ?>">
                                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                                  <img alt="banner posteo <?php the_title(); ?>" src="<?php  echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="image">
                                                <?php else: ?>
                                                    <img alt="banner por defecto posteo" src="<?php echo get_stylesheet_directory_uri() ?>/img/default-post.jpg" alt="image">
                                               <?php endif; ?>
                                                </a>
                                            </div>
                                            
                                            <div class="col-7 my-auto box-detail">
                                               <a href="<?php the_permalink(); ?>">
                                                <h2><?php the_title(); ?></h2>
                                                <p><?php echo get_the_date( 'd/m/Y' ); ?></p>
                                               </a>
                                            </div>
                                        
                                    </div>
                                <?php endwhile; ?>
                              
                            </div>

                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--main page content end-->


<?php
get_footer();
?>